/*
 * battstat/powersave.c - Powersave backend for the battery status applet
 *
 * Robert Love  <rml@novell.com>
 *
 * Copyright (C) 2004 Novell, Inc.
 *
 * Licensed under the GNU GPL version 2.  See COPYING.
 */

#include <gnome.h>
#include <powerlib.h>

#ifdef __FreeBSD__
#include <machine/apm_bios.h>
#elif defined(__NetBSD__) || defined(__OpenBSD__)
#include <sys/param.h>
#include <machine/apmvar.h>
#elif __linux__
#include <apm.h>
#endif

#include "powerlib.h"

/**
 * powersave_init - is the Powersave backend working?
 *
 * Returns TRUE if everything looks cool and FALSE otherwise.  Uses
 * getACAdapterStatus ().  It would be nice if there were an isDaemonRunning ()
 * but, hey, a beer would be nice, too.
 */
gboolean powersave_init (void)
{
	if (getACAdapterStatus () == AC_UNKNOWN)
		return FALSE;
	return TRUE;
}

/**
 * powersave_get - Use Powersave to fill out the apm_info structure
 *
 * @apminfo:	apm_info structure that we wish to fill out
 *
 * ->ac_line_status is one when on AC and zero otherwise
 * ->battery_percentage is the percentage of remaining battery strength
 * ->battery_status is 0 for high, 1 for low, and 2 for critical
 * ->battery_flags & 0x8 is nonzero when charging, zero otherwise
 * ->battery_time is the minutes remaining of battery life
 */
void powersave_get (struct apm_info *apminfo)
{
	struct BatteryGeneral bat_gen;
	struct BatteryDetailed bat_det;

	g_assert (apminfo);

	if (getACAdapterStatus () == AC_ONLINE)
		apminfo->ac_line_status = 1;
	else
		apminfo->ac_line_status = 0;

	getBatteriesInfo (&bat_gen);

	apminfo->battery_percentage = bat_gen.remPerc;

	/*
	 * For ACPI, we can get the actual capacity_state.  For APM, we cannot
	 * through Powersave, even though we can in reality, so we fake it.
	 */
	if (getBatteryDetailedInfo (0, &bat_det) < 0) {
		if (bat_det.capacity_state == STATE_OK)
			apminfo->battery_status = 0;
		else if (bat_det.capacity_state == STATE_LOW)
			apminfo->battery_status = 1;
		else
			apminfo->battery_status = 2;
	} else {
		if (bat_gen.remPerc > 50)
			apminfo->battery_status = 0;
		else if (bat_gen.remPerc > 15)
			apminfo->battery_status = 1;
		else
			apminfo->battery_status = 0;
	}

	if (bat_gen.charging_state == CHARG_STATE_CHARGING )
		apminfo->battery_flags = 0x8;
	else
		apminfo->battery_flags = 0;

	if (!apminfo->ac_line_status)
		apminfo->battery_time = bat_gen.remMins;
	else
		apminfo->battery_time = -2;
}
