/* Notification Battery
 * Copyright (C) 2004 Benjamin Kahn <xkahn@zoned.net>
 * Copyright (C) 2001 Bastien Nocera <hadess@hadess.net>
 *
 * notibat.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
 * USA.
 */

#include <config.h>

#include <stdio.h>

#ifdef __FreeBSD__
#include <machine/apm_bios.h>
#elif defined(__NetBSD__) || defined(__OpenBSD__)
#include <sys/param.h>
#include <machine/apmvar.h>
#elif __linux__
#include <apm.h>
#endif

#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/sysctl.h>

#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>

#include <sys/file.h>
#include <X11/X.h>

/* Gnome headers */
#include <gdk/gdkx.h>
#include <gnome.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>
#include "eggtrayicon.h"

#include "notibat.h"

#include "acpi-linux.h"

typedef struct {
	GladeXML *xml;
	GtkWidget *dialog;
	GConfClient *conf_client;
	guint dialog_timeout;
        GtkWidget *image;
  
        /* Battery stuff */
        int acpiwatch;

        /* Warning dialog */
        int low_battery;
        GtkDialog *warning;
        gulong warning_response_handler;
        gulong warning_unmap_handler;
        gulong warning_delete_handler;

	/* Tray icon */
        int shown;
	EggTrayIcon *tray_icon;
	GtkTooltips *tray_icon_tooltip;
	GtkWidget *popup_menu;
} Notibat;

static void notibat_error_dialog (char        *msg);
gint notibat_checkbatt (gpointer data);
static void notibat_image_set (Notibat *nb, char *icon);

#if defined(__NetBSD__) || defined(__OpenBSD__)
struct apm_power_info apminfo;
#else
struct apm_info apminfo;
#endif

#ifdef __linux__
struct acpi_info acpiinfo;
gboolean using_acpi;
int acpi_count;
#endif

gboolean using_powersave;

#ifdef __FreeBSD__
void
apm_readinfo (Notibat * nb)
{
  /* This is how I read the information from the APM subsystem under
     FreeBSD.  Each time this functions is called (once every second)
     the APM device is opened, read from and then closed.
  */
  int fd;
  if (DEBUG) g_print("apm_readinfo() (FreeBSD)\n");

  fd = open(APMDEVICE, O_RDONLY);
  if (fd == -1) cleanup (nb, 1);

  if (ioctl(fd, APMIO_GETINFO, &apminfo) == -1)
    err(1, "ioctl(APMIO_GETINFO)");

  close(fd);
}
#elif defined(__NetBSD__) || defined(__OpenBSD__)
void
apm_readinfo (Notibat * nb)
{
  /* Code for OpenBSD by Joe Ammond <jra@twinight.org>. Using the same
     procedure as for FreeBSD.
  */
  int fd;
#if defined(__NetBSD__)
  if (DEBUG) g_print("apm_readinfo() (NetBSD)\n");
#else /* __OpenBSD__ */
  if (DEBUG) g_print("apm_readinfo() (OpenBSD)\n");
#endif

  fd = open(APMDEVICE, O_RDONLY);
  if (fd == -1) cleanup (nb, 1);
  if (ioctl(fd, APM_IOC_GETPOWER, &apminfo) == -1)
    err(1, "ioctl(APM_IOC_GETPOWER)");
  close(fd);
}
#elif __linux__

// Declared in acpi-linux.c
gboolean acpi_linux_read(struct apm_info *apminfo, struct acpi_info *acpiinfo);

gboolean acpi_callback (GIOChannel * chan, GIOCondition cond, gpointer data)
{
  if (cond & (G_IO_ERR | G_IO_HUP)) {
    acpi_linux_cleanup(&acpiinfo);
    apminfo.battery_percentage = -1;
    return FALSE;
  }
  
  if (acpi_process_event(&acpiinfo)) {
    acpi_linux_read(&apminfo, &acpiinfo);
    notibat_checkbatt(data);
  }
  return TRUE;
}

void
apm_readinfo (Notibat * nb)
{
  /* Code for Linux by Thomas Hood <jdthood@mail.com>. apm_read() will
     read from /proc/... instead and we do not need to open the device
     ourselves.
  */

#ifdef HAVE_POWERSAVE
  /* Use Powersave if we have that available to us */
  if (using_powersave) {
    powersave_get(&apminfo);
  } else
#endif
    
  /* ACPI support added by Lennart Poettering <lennart@poettering.de> 10/27/2001
   * Updated by David Moore <dcm@acm.org> 5/29/2003 to poll less and
   *   use ACPI events. */
  if (using_acpi && acpiinfo.event_fd >= 0) {
    if (acpi_count <= 0) {
      /* Only call this one out of 30 calls to apm_readinfo() (every 30 seconds)
       * since reading the ACPI system takes CPU cycles. */
      acpi_count=30;
      acpi_linux_read(&apminfo, &acpiinfo);
    }
    acpi_count--;
  }
  /* If we lost the file descriptor with ACPI events, try to get it back. */
  else if (using_acpi) {
      if (acpi_linux_init(&acpiinfo)) {
          nb->acpiwatch = g_io_add_watch (acpiinfo.channel,
              G_IO_IN | G_IO_ERR | G_IO_HUP,
              acpi_callback, nb);
          acpi_linux_read(&apminfo, &acpiinfo);
      }
  }
  else
    apm_read(&apminfo);
}

#else
void
apm_readinfo (Notibat * nb)
{
  g_print("apm_readinfo() (Generic)\n");
  g_print(
	  /* Message displayed if user tries to run applet
	     on unsupported platform */
	  _("Your platform is not supported!\n"));
  g_print(_("The monitor will not work properly (if at all).\n"));
}
#endif 

static void init_tray (Notibat *notibat);

char *
gettext_noop (char *a) {
  return a;
}

static void
notibat_exit (Notibat *nb)
{
	exit (0);
}

static void
notibat_error_dialog (char * msg)
{
	GtkWidget *error_dialog;

	error_dialog =
	    gtk_message_dialog_new (NULL,
			    GTK_DIALOG_MODAL,
			    GTK_MESSAGE_ERROR,
			    GTK_BUTTONS_OK,
			    "%s", msg);
	gtk_dialog_set_default_response (GTK_DIALOG (error_dialog),
			GTK_RESPONSE_OK);
	gtk_widget_show (error_dialog);
	gtk_dialog_run (GTK_DIALOG (error_dialog));
	gtk_widget_destroy (error_dialog);
}

static void
warning_response_handler (GtkDialog *dialog,
			  gint response_id,
			  gpointer data)
{
  Notibat *nb = data;

  if (!nb->warning) return;

  g_signal_handler_disconnect (dialog, nb->warning_response_handler);
  g_signal_handler_disconnect (dialog, nb->warning_delete_handler);
  g_signal_handler_disconnect (dialog, nb->warning_unmap_handler);

  g_object_unref (dialog);
  gtk_object_destroy (GTK_OBJECT (dialog));

  nb->warning = NULL;
}

static gint
warning_delete_handler (GtkDialog *dialog,
			GdkEventAny *event,
			gpointer data)
{
  Notibat *nb = data;

  g_signal_handler_disconnect (dialog, nb->warning_response_handler);
  g_signal_handler_disconnect (dialog, nb->warning_delete_handler);
  g_signal_handler_disconnect (dialog, nb->warning_unmap_handler);

  g_object_unref (dialog);

  nb->warning = NULL;

  return FALSE;
}

static void
warning_unmap_handler (GtkDialog *dialog, gpointer data)
{
  Notibat *nb = data;

  g_signal_handler_disconnect (dialog, nb->warning_response_handler);
  g_signal_handler_disconnect (dialog, nb->warning_delete_handler);
  g_signal_handler_disconnect (dialog, nb->warning_unmap_handler);

  g_object_unref (dialog);

  nb->warning = NULL;
}

GtkDialog *
hig_error_new (GtkWindow *parent, const char *error, const char *explain)
{
	GtkWidget *hbox, *w;
	char *tmp, *domain, *id;
	GString *out;
	GPtrArray *args;
	GtkDialog *dialog;

	dialog = (GtkDialog *)gtk_dialog_new();
	gtk_dialog_set_has_separator(dialog, FALSE);

	if (parent)
		gtk_window_set_transient_for((GtkWindow *)dialog, parent);


	/*	w = gtk_label_new(NULL);
	gtk_label_set_selectable((GtkLabel *)w, TRUE);
	gtk_label_set_line_wrap((GtkLabel *)w, TRUE);
	gtk_label_set_markup((GtkLabel *)w, tmp);
	gtk_widget_show(w);
	gtk_box_pack_start((GtkBox *)dialog->vbox, w, TRUE, TRUE, 12);
	*/	
	gtk_dialog_add_button(dialog, GTK_STOCK_OK, GTK_RESPONSE_OK);
	gtk_dialog_set_default_response (dialog, GTK_RESPONSE_OK);

	gtk_window_set_destroy_with_parent((GtkWindow *)dialog, TRUE);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_container_set_border_width((GtkContainer *)hbox, 12);

	w = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start((GtkBox *)hbox, w, TRUE, TRUE, 12);

	tmp = g_strdup_printf("<span size=\"larger\" weight=\"bold\">%s</span>\n\n%s", error, explain);
	w = gtk_label_new(NULL);
	gtk_label_set_selectable((GtkLabel *)w, TRUE);
	gtk_label_set_line_wrap((GtkLabel *)w, TRUE);
	gtk_label_set_markup((GtkLabel *)w, tmp);
	gtk_box_pack_start((GtkBox *)hbox, w, FALSE, FALSE, 0);
	gtk_widget_show_all(hbox);

	gtk_box_pack_start((GtkBox *)dialog->vbox, hbox, TRUE, TRUE, 0);

	out = g_string_new("");

	gtk_window_set_title((GtkWindow *)dialog, _("Battery Warning"));

	return dialog;
}

void
battery_low_warning (Notibat *nb)
{
  GtkDialog *dialog;

  if (nb->warning != NULL) {
    gtk_window_present ((GtkWindow *) nb->warning);
    return;
  }

  dialog = nb->warning = hig_error_new(NULL, "Battery is critically low", "The amount of charge left in the battery is very low and the computer is not plugged in.  Save your work and shutdown or plug the computer in.");

  g_object_ref (dialog);

  gtk_widget_show (GTK_WIDGET (dialog));

  nb->warning_response_handler =
    g_signal_connect (dialog,
                      "response",
                      G_CALLBACK (warning_response_handler),
                      nb);

  nb->warning_unmap_handler =
    g_signal_connect (dialog,
                      "unmap",
                      G_CALLBACK (warning_unmap_handler),
                      nb);

  nb->warning_delete_handler =
    g_signal_connect (dialog,
                      "delete_event",
                      G_CALLBACK (warning_delete_handler),
                      nb);

}

static void
selection_get_func (GtkClipboard *clipboard, GtkSelectionData *selection_data,
		guint info, gpointer user_data_or_owner)
{
}

static void
selection_clear_func (GtkClipboard *clipboard, gpointer user_data_or_owner)
{       
	return;
}

#define SELECTION_NAME "_NOTIBAT_SELECTION"

static gboolean
notibat_get_lock (Notibat *nb)
{
	gboolean result = FALSE;
	GtkClipboard *clipboard;
	Atom clipboard_atom = gdk_x11_get_xatom_by_name (SELECTION_NAME);
	static const GtkTargetEntry targets[] = {
		{ SELECTION_NAME, 0, 0 }
	};

	XGrabServer (GDK_DISPLAY());

	if (XGetSelectionOwner (GDK_DISPLAY(), clipboard_atom) != None)
		goto out;

	clipboard = gtk_clipboard_get (gdk_atom_intern (SELECTION_NAME, FALSE));

	if (!gtk_clipboard_set_with_data  (clipboard, targets,
				G_N_ELEMENTS (targets),
				selection_get_func,
				selection_clear_func, NULL))
		goto out;

	result = TRUE;

out:
	XUngrabServer (GDK_DISPLAY());
	gdk_flush();

	return result;
}

/*
static void
execute (char *cmd, gboolean sync)
{
	gboolean retval;

	if (sync != FALSE)
		retval = g_spawn_command_line_sync
			(cmd, NULL, NULL, NULL, NULL);
	else
		retval = g_spawn_command_line_async (cmd, NULL);

	if (retval == FALSE)
	{
		char *msg;

		msg = g_strdup_printf
			(_("Couldn't execute command: %s\n"
			   "Verify that this command exists."),
			 cmd);

		notibat_error_dialog (msg);
		g_free (msg);
	}
}
*/

/*
static void
execute_this_or_that (char *cmd1, char *cmd2)
{
	if (g_spawn_command_line_async (cmd1, NULL) == FALSE)
	{
		if (g_spawn_command_line_async (cmd2, NULL) == FALSE)
		{
			char *msg;

			msg = g_strdup_printf
				(_("Couldn't execute either command: %s\n"
				   "or command: %s\n"
				   "Verify that at least one of these commands"
				   " exist."),
				 cmd1, cmd2);

			notibat_error_dialog (msg);
			g_free (msg);
		}
	}
}

static void
notibat_play_sound (Notibat *notibat)
{
	char *soundfile, *command;

	soundfile = gconf_client_get_string (notibat->conf_client,
			"/apps/notibat/soundfile_name", NULL);
	if ((soundfile == NULL) || (strcmp (soundfile, "") == 0)) 
		return;

	if (g_file_test ("/usr/bin/esdplay",
			(G_FILE_TEST_EXISTS | G_FILE_TEST_IS_EXECUTABLE)))
	{
		command = g_strdup_printf ("/usr/bin/esdplay %s",
				soundfile);
	} else if (g_file_test ("/usr/bin/play",
			(G_FILE_TEST_EXISTS | G_FILE_TEST_IS_EXECUTABLE)))
	{
		command = g_strdup_printf ("/usr/bin/play %s",
				soundfile);
	} else {
		return;
	}

	execute (command, FALSE);
	g_free (command);
}

*/

/*
static char*
permission_problem_string (const char *files)
{
	return g_strdup_printf (_("Permissions on the file %s are broken\n"
				"Please check Notibat's documentation, correct "
				"the problem and restart Notibat."), files);
}

static void
_error_dialog (char        *msg)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new(NULL,
			0,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_OK,
			msg);

	gtk_dialog_run (GTK_DIALOG(dialog));
	gtk_widget_destroy (dialog);
}
*/

gint
notibat_checkbatt (gpointer data)
{
  Notibat *nb = (Notibat *) data;
  static char *oldtip = NULL;
  guint batt_life;
  guint acline_status;
  guint batt_state;
  guint charging;
  gboolean batterypresent;
  gchar new_label[80];
  gchar *status[]={
    /* The following four messages will be displayed as tooltips over
     the battery meter.  
     High = The APM BIOS thinks that the battery charge is High.*/
    gettext_noop ("high"),
    /* Low = The APM BIOS thinks that the battery charge is Low.*/
    gettext_noop ("low"),
    /* Critical = The APM BIOS thinks that the battery charge is Critical.*/
    gettext_noop ("critical"),
    /* Charging = The APM BIOS thinks that the battery is recharging.*/
    gettext_noop ("charging")
  };

  /*
    Mental note to self explaining the expected values below.
    acline_status: 
      0 = Running on battery
      1 = Running on AC

    batt_life:
      0-100 Percentage of battery charge left

    batt_state:
      0 = High
      1 = Low
      2 = Critical
      3 = Charging
  */

   apm_readinfo (nb);
   batterypresent = TRUE;
#ifdef __FreeBSD__
   acline_status = apminfo.ai_acline ? 1 : 0;
   batt_state = apminfo.ai_batt_stat;
   batt_life = apminfo.ai_batt_life;
   charging = (batt_state == 3) ? TRUE : FALSE;
#elif defined(__NetBSD__) || defined(__OpenBSD__)
   acline_status = apminfo.ac_state ? 1 : 0;
   batt_state = apminfo.battery_state;
   batt_life = apminfo.battery_life;
   charging = (batt_state == 3) ? TRUE : FALSE;
#elif __linux__
   acline_status = apminfo.ac_line_status ? 1 : 0;
   batt_state = apminfo.battery_status;
   batt_life = (guint) apminfo.battery_percentage;
   charging = (apminfo.battery_flags & 0x8) ? TRUE : FALSE;
#else
   acline_status = 1;
   batt_state = 0;
   batt_life = 100;
   charging = TRUE;
   batterypresent = FALSE;
#endif
  if(batt_state > 3) {
    batt_state = 0;
    batterypresent = FALSE;
  }
  if(batt_life == (guint)-1) {
    batt_life = 0;
    batterypresent = FALSE;
  }
  if(batt_life > 100) batt_life = 100;
  if(batt_life == 100) charging = FALSE;
  if(!acline_status) charging = FALSE;
   
  /*  printf ("acline_status: %s, batt_state: %d, batt_life: %d, charging: %s\n", acline_status == 1 ? "plug" : "battery", batt_state, batt_life, charging == 1 ? "charging" : "not");*/

  if (!batterypresent || (batt_life == 0 && batt_state == 0 && charging == 0)) { /* No battery */
    if (nb->shown)
      /*      printf ("Hiding!\n"); */
      gtk_widget_hide_all (GTK_WIDGET (nb->tray_icon));
      nb->shown = FALSE;
  } else {
    if (!nb->shown)
      /*      printf ("Showing!\n"); */
      gtk_widget_show_all (GTK_WIDGET (nb->tray_icon));
      nb->shown = TRUE;
  }
  
  if (!acline_status && batt_life < 5) { /* Danger!  Danger! */
    if (!nb->low_battery) { /* Raise an alert window */
      gnome_triggers_do ("", NULL, "battstat_applet", "LowBattery", NULL); /* Tell GNOME */
      nb->low_battery = TRUE;
      battery_low_warning (nb);
	}
  } else {
    if (nb->low_battery) { /* Lower the alert window */
      nb->low_battery = FALSE;
      warning_response_handler (nb->warning, 0, (gpointer) nb);
      nb->warning = FALSE;
    }
  }
      

  if (acline_status) 
    {
      if (batt_life < 5)
	notibat_image_set (nb, "notibat-16-plug-0.png");
      else if (batt_life < 25)
	notibat_image_set (nb, "notibat-16-plug-25.png");
      else if (batt_life < 50)
	notibat_image_set (nb, "notibat-16-plug-50.png");
      else if (batt_life < 75)
	notibat_image_set (nb, "notibat-16-plug-75.png");
      else
	notibat_image_set (nb, "notibat-16-plug-100.png");
    } else {
      if (batt_life < 5) { /* Danger!  Danger! */
	notibat_image_set (nb, "notibat-16-0.png");
      } else if (batt_life < 25)
	notibat_image_set (nb, "notibat-16-25.png");
      else if (batt_life < 50)
	notibat_image_set (nb, "notibat-16-50.png");
      else if (batt_life < 75)
	notibat_image_set (nb, "notibat-16-75.png");
      else
	notibat_image_set (nb, "notibat-16-100.png");
    }

  snprintf(new_label, 80, _("Battery is %s (%d%%)"),
	   status[batt_state], batt_life);

  if (!oldtip || strcmp (oldtip, new_label) != 0) {
  
    gtk_tooltips_set_tip (nb->tray_icon_tooltip,
			  GTK_WIDGET (nb->tray_icon),
			  new_label,
			  NULL);

    g_free (oldtip);
    oldtip = g_strdup (new_label);
  }

  return TRUE;

}

static void
notibat_image_set (Notibat *nb, char *icon)
{
	GtkWidget *image;
	char *path;

	image = nb->image;/* glade_xml_get_widget (nb->xml, "image1"); */
	g_return_if_fail (image != NULL);

	path = g_strconcat (NOTIBAT_DATA, icon, NULL);

	/* 	printf ("path: %s\n", path); */

	gtk_image_set_from_file (GTK_IMAGE(image),
				 path);

	g_free (path);
}

/*
static void
prefs_activated (GtkMenuItem *menuitem, gpointer user_data)
{
	execute ("notibat-properties", FALSE);
}
*/

static void
about_activated (GtkMenuItem *menuitem, gpointer user_data)
{
	static GtkWidget *about = NULL;
	GdkPixbuf *pixbuf = NULL;
	const gchar *authors[] = { "Benjamin Kahn <xkahn@ximian.com>", 
				   "Lennart Poettering <lennart@poettering.de> (Linux ACPI support)",
				   "Seth Nickell <snickell@stanford.edu> (GNOME2 port)",
				   "Robert Love <rml@novell.com> (Powersave support)",
				   NULL };
	const gchar *documenters[] = { NULL };
	const gchar *translator_credits = _("translator_credits");

	if (about != NULL)
	{
		gdk_window_raise (about->window);
		gdk_window_show (about->window);
		return;
	}

	pixbuf = gdk_pixbuf_new_from_file (NOTIBAT_DATA "notibat-48.png", NULL);

	about = gnome_about_new(_("Notibat"), VERSION,
			"Copyright \xc2\xa9 2004 Benjamin Kahn",
			_("Notification Area Battery Monitor"),
			(const char **)authors,
			(const char **)documenters,
			strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
			pixbuf);
	
	if (pixbuf != NULL)
		gdk_pixbuf_unref (pixbuf);
	
	g_signal_connect (G_OBJECT (about), "destroy",
			G_CALLBACK (gtk_widget_destroyed), &about);
	g_object_add_weak_pointer (G_OBJECT (about), (void**)&(about));

	gtk_widget_show(about);
}

static void
quit_activated (GtkMenuItem *menuitem, gpointer user_date)
{
  GnomeClient *master;
  GnomeClientFlags flags;
  
  master = gnome_master_client ();
  flags = gnome_client_get_flags (master);
  if (flags & GNOME_CLIENT_IS_CONNECTED) {

    gnome_client_set_restart_style (master,
				    GNOME_RESTART_NEVER);
    gnome_client_flush (master);
  }
  
  exit (0);
}



static gboolean
tray_icon_release (GtkWidget *widget, GdkEventButton *event, Notibat *notibat)
{
	if (event->button == 3)
	{
		gtk_menu_popdown (GTK_MENU (notibat->popup_menu));
		return FALSE;
	}

	return TRUE;
}


static gboolean
tray_icon_press (GtkWidget *widget, GdkEventButton *event, Notibat *notibat)
{
	if (event->button == 3)
	{
		gtk_menu_popup (GTK_MENU (notibat->popup_menu), NULL, NULL, NULL,
				NULL, event->button, event->time);
		return TRUE;
	}

	return FALSE;
}

static gboolean
tray_destroyed (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	init_tray ((Notibat *) user_data);
	return TRUE;
}

static void
init_tray (Notibat *notibat)
{
	GtkWidget *image, *evbox, *item;

	notibat->tray_icon = egg_tray_icon_new ("Battery Monitor");
	image = gtk_image_new_from_file (NOTIBAT_DATA "notibat-16.png");
	notibat->image = image;

	notibat->tray_icon_tooltip = gtk_tooltips_new ();
	gtk_tooltips_set_tip (notibat->tray_icon_tooltip,
			GTK_WIDGET (notibat->tray_icon),
			_("Notibat"),
			NULL);

	/* Event box */
	evbox = gtk_event_box_new ();
	g_signal_connect (G_OBJECT (evbox), "button_press_event",
			G_CALLBACK (tray_icon_press), (gpointer) notibat);
	g_signal_connect (G_OBJECT (evbox), "button_release_event",
			G_CALLBACK (tray_icon_release), (gpointer) notibat);

	/* Popup menu */
	notibat->popup_menu = gtk_menu_new ();
	/*	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_PREFERENCES,
			NULL);
	g_signal_connect (G_OBJECT (item), "activate",
			G_CALLBACK (prefs_activated), (gpointer) notibat);
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (notibat->popup_menu), item);
	*/
	item = gtk_image_menu_item_new_from_stock (GNOME_STOCK_ABOUT,
			NULL);
	g_signal_connect (G_OBJECT (item), "activate",
			G_CALLBACK (about_activated), (gpointer) notibat);
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (notibat->popup_menu), item);

	
	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT,
                        NULL);
	g_signal_connect (G_OBJECT (item), "activate",
			G_CALLBACK(quit_activated), (gpointer) notibat);
	gtk_widget_show(item);
	gtk_menu_shell_append (GTK_MENU_SHELL (notibat->popup_menu), item);      

 
	gtk_container_add (GTK_CONTAINER (evbox), image);
	gtk_container_add (GTK_CONTAINER (notibat->tray_icon), evbox);

	if (notibat->shown) 
	    gtk_widget_show_all (GTK_WIDGET (notibat->tray_icon));
	  else
	    gtk_widget_hide_all (GTK_WIDGET (notibat->tray_icon));

	g_signal_connect (G_OBJECT (notibat->tray_icon), "destroy-event",
			G_CALLBACK (tray_destroyed), (gpointer) notibat);
}

static void
init_sm (Notibat *notibat)
{
	GnomeClient *master;
	GnomeClientFlags flags;

	master = gnome_master_client ();
	flags = gnome_client_get_flags (master);
	if (flags & GNOME_CLIENT_IS_CONNECTED) {
#ifdef DEBUG
		gnome_client_set_restart_style (master,
				GNOME_RESTART_NEVER);
#else
		gnome_client_set_restart_style (master,
				GNOME_RESTART_IMMEDIATELY);
#endif
		gnome_client_flush (master);
	}

	g_signal_connect (GTK_OBJECT (master), "die",
			G_CALLBACK (notibat_exit), notibat);
}

int
main (int argc, char *argv[])
{
	Notibat *nb;
        struct stat statbuf;

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	gnome_program_init ("notibat", VERSION,
			LIBGNOMEUI_MODULE,
			argc, argv,
			NULL);

	nb = g_new0 (Notibat, 1);
	nb->conf_client = NULL;
	nb->shown = 0;
	nb->low_battery = FALSE;

	if (notibat_get_lock (nb) == FALSE)
	{
		g_print ("Daemon already running, exiting...\n");
		notibat_exit (nb);
	}

	glade_gnome_init ();
	nb->xml = glade_xml_new (NOTIBAT_DATA "notibat.glade", NULL, NULL);

	if (nb->xml == NULL) {
		notibat_error_dialog (_("Couldn't load the Glade file.\n"
				"Make sure that this daemon is properly"
				" installed."));
		exit (1);
	}

	nb->dialog = glade_xml_get_widget (nb->xml, "dialog");

	nb->conf_client = gconf_client_get_default ();
	gconf_client_add_dir (nb->conf_client,
			"/apps/notibat",
			GCONF_CLIENT_PRELOAD_ONELEVEL,
			NULL);

	init_sm (nb);
	
#ifdef __linux__
# ifdef HAVE_POWERSAVE
	if (powersave_init())
	  using_powersave = TRUE;
	else
# endif   
	  if (acpi_linux_init(&acpiinfo)) {
	    using_acpi = TRUE;
	    acpi_count = 0;
	  }
	  else
	    using_acpi = FALSE;
	
	/* If ACPI, Powersave, nor APM are working, but ACPI does seem to be
	 * installed, warn the user how to get ACPI working. */
	if (!using_powersave && !using_acpi && (apm_exists() == 1) &&
	    (stat("/proc/acpi", &statbuf) == 0)) {
	  notibat_error_dialog (
				_("Can't access ACPI events in /var/run/acpid.socket! "
				  "Make sure the ACPI subsystem is working and "
				  "the acpid daemon is running."));
	  using_acpi = TRUE;
	  acpi_count = 0;
	}
#endif
	apm_readinfo (NULL);
	
#ifdef __FreeBSD__
	if(apminfo.ai_status == 0) cleanup (nb, 2);
#endif
	
#ifdef __linux__
	/* Watch for ACPI events and handle them immediately with acpi_callback(). */
	if (using_acpi && acpiinfo.event_fd >= 0) {
	  nb->acpiwatch = g_io_add_watch (acpiinfo.channel,
					  G_IO_IN | G_IO_ERR | G_IO_HUP,
					  acpi_callback, nb);
	}
#endif
	init_tray (nb);
	gtk_widget_realize (nb->dialog);
	nb->dialog_timeout = g_timeout_add (1000, notibat_checkbatt, nb);
	
	gtk_main ();
	
	return 0;
}


