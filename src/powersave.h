#ifndef _BATTSTAT_POWERSAVE_H
#define _BATTSTAT_POWERSAVE_H

gboolean powersave_init (void);
void powersave_read (struct apm_info *);

#endif /* _BATSTATT_POWERSAVE_H */
