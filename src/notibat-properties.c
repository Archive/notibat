/* NOTIBAT
 * Copyright (C) 2001, 2002 Bastien Nocera <hadess@hadess.net>
 *
 * notibat-properties.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, 
 * USA.
 */

#include <config.h>

#include <sys/file.h>
#include <gnome.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>
#include <gdk/gdkx.h>

#include "notibat.h"

GConfClient *conf_client;
GdkPixbuf *enabled, *disabled;

#define SELECTION_NAME "_NOTIBAT_SELECTION"

static gboolean
is_running (void)
{
	gboolean result = FALSE;
	Atom clipboard_atom = gdk_x11_get_xatom_by_name (SELECTION_NAME);

	XGrabServer (GDK_DISPLAY());

	if (XGetSelectionOwner (GDK_DISPLAY(), clipboard_atom) != None)
		result = TRUE;

	XUngrabServer (GDK_DISPLAY());
	gdk_flush();

	return result;
}

static void
response_cb (GtkDialog *dialog, gint response, gpointer data)
{
	gtk_main_quit ();
}

static void
notibat_error (char * msg)
{
	GtkWidget *error_dialog;

	error_dialog =
		gtk_message_dialog_new (NULL,
				GTK_DIALOG_MODAL,
				GTK_MESSAGE_ERROR,
				GTK_BUTTONS_OK,
				"%s", msg);
	gtk_dialog_set_default_response (GTK_DIALOG (error_dialog),
			GTK_RESPONSE_OK);
	gtk_widget_show (error_dialog);
	gtk_dialog_run (GTK_DIALOG (error_dialog));
	gtk_widget_destroy (error_dialog);
}

static GtkWidget
*init_gui (void)
{
	GladeXML *xml;
	GtkWidget *window, *treeview, *checkbox;
	GdkPixbuf *pixbuf;

	xml = glade_xml_new (NOTIBAT_DATA "notibat-properties.glade", NULL, NULL);

	window = glade_xml_get_widget (xml, "window");
	g_signal_connect (G_OBJECT (window), "response",
			G_CALLBACK (response_cb), NULL);

	enabled = gtk_widget_render_icon (window, GTK_STOCK_APPLY,
			GTK_ICON_SIZE_MENU, NULL);
	disabled = gtk_widget_render_icon (window, GTK_STOCK_CANCEL,
			GTK_ICON_SIZE_MENU, NULL);

	pixbuf = gdk_pixbuf_new_from_file (NOTIBAT_DATA "notibat-48.png", NULL);
	if (pixbuf != NULL)
		gtk_window_set_icon (GTK_WINDOW (window), pixbuf);

	if (is_running () == FALSE)
		g_spawn_command_line_async ("notibat", NULL);

	gtk_widget_show_all (window);

	return window;
}

int
main (int argc, char *argv[])
{
	GtkWidget *dialog;

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	gnome_program_init ("notibat-properties", VERSION,
			LIBGNOMEUI_MODULE,
			argc, argv,
			NULL);

	glade_gnome_init ();

	conf_client = gconf_client_get_default ();
	gconf_client_add_dir (conf_client,
			"/apps/notibat",
			GCONF_CLIENT_PRELOAD_ONELEVEL,
			NULL);

	dialog = init_gui ();
	gtk_main ();

	return 0;
}

